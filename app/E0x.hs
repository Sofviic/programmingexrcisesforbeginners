module E0x (
    solve01,

    gen03,solve03,

    gen05,solve05,
    gen06,solve06,
    gen07,solve07,
    solve08,
    gen09,solve09,
    dir
) where

import Control.Monad
import System.Random
import Data.List (sort,group)

dir :: FilePath -> FilePath
dir = ("src/0x/"<>)

fibs :: [Int]
fibs = 0 : 1 : ap (zipWith (+)) tail fibs

solve01 :: IO ()
solve01 = writeFile (dir "01s.txt") . unlines . take 90 $ zipWith (\i f -> (\ii -> replicate (2 - length ii) '0' <> ii) (show i) <> ": " <> show f) [0..] fibs

gen03 :: IO ()
gen03 = do
        val <- forM [1..100000] $ const $ randomRIO (' ','~')
        writeFile (dir "03.txt") val

solve03 :: IO ()
solve03 = do
        s <- readFile (dir "03.txt")
        writeFile (dir "03s.txt") . unlines $ fmap (\c -> "'" <> pure c <> "': " <> (show . length . filter (==c)) s) [' '..'~']

gen05 :: IO ()
gen05 = do
        val <- forM [1..1000] $ const (randomIO :: IO Int)
        writeFile (dir "05.txt") . unlines . fmap (show . (`mod` 1000)) $ val

solve05 :: IO ()
solve05 = do
        c <- readFile (dir "05.txt")
        writeFile (dir "05s.txt") . show . sum . fmap (read :: String -> Int) . lines $ c

unwords' :: String -> String -> String
unwords' = (<>) . (<> " ")

gen06 :: IO ()
gen06 = do
        val1 <- (fmap.fmap) (show . (`mod` 100)) . forM [1..1000] $ const (randomIO :: IO Int)
        val2 <- (fmap.fmap) (show . (`mod` 100)) . forM [1..1000] $ const (randomIO :: IO Int)
        writeFile (dir "06.txt") . unlines $ zipWith unwords' val1 val2

solve06 :: IO ()
solve06 = do
        c <- readFile (dir "06.txt")
        writeFile (dir "06s.txt") . show . sum . fmap product . (fmap.fmap) (read :: String -> Int) . fmap words. lines $ c

scramble :: [Int] -> [String] -> [String] -> [String]
scramble ns rs = zipWith (\n l -> if n `elem` ns then rs !! n else l) [0..]

gen07 :: IO ()
gen07 = do
        c <- fmap lines $ readFile (dir "07a.txt")
        rand1 <- (fmap.fmap) (`mod` 100) . forM [1..100] $ const (randomIO :: IO Int)
        rand2 <- (fmap.fmap) (`mod` 100) . forM [1..20] $ const (randomIO :: IO Int)
        let r = fmap (c !!) rand1
        writeFile (dir "07b.txt") . unlines $ scramble rand2 r c

diff :: [String] -> [String] -> [Int]
diff as bs = filter (/= (-1)) . zipWith (\n l -> if l then (-1) else n) [0..] $ zipWith (==) as bs

solve07 :: IO ()
solve07 = do
        a <- fmap lines $ readFile (dir "07a.txt")
        b <- fmap lines $ readFile (dir "07b.txt")
        writeFile (dir "07s.txt") . unlines . fmap show $ diff a b

solve08 :: IO ()
solve08 = do
        c <- readFile (dir "08.txt")
        if length (filter (\c -> c > '\127') c) < 1 
                then putStrLn "only ascii" 
                else error "non-ascii"
        writeFile (dir "08s.txt") . unlines . sort . lines $ c

modAround :: (Int, Int) -> Int -> Int
modAround (a,b) = (a+) . (`mod` (b-a))

gen09 :: IO ()
gen09 = do
        val1 <- (fmap.fmap) (`mod` 100) . forM [1..10000] $ const (randomIO :: IO Int)
        val2 <- (fmap.fmap) (toEnum . modAround (32,127)) . forM [1..10000] $ const (randomIO :: IO Int)
        writeFile (dir "09.txt") . mconcat $ zipWith replicate val1 val2

runLengthEncode :: String -> [(Char,Int)]
runLengthEncode = fmap (\l -> (head l,length l)) . group

runLengthDecode :: [(Char,Int)] -> String
runLengthDecode = mconcat . fmap (uncurry $ flip replicate)

solve09 :: IO ()
solve09 = do
        c <- readFile (dir "09.txt")
        if (runLengthDecode . runLengthEncode) c == id c 
                then putStrLn "successful encode-decode" 
                else error "no longer the same as original file"
        writeFile (dir "09s.txt") . mconcat . fmap (\(c,i) -> [c] ++ ":" ++ show i ++ ",") . runLengthEncode $ c
