module L0x (
    gen00,solve00,
    --gen01,solve01,
    --gen02,solve02,
    --gen03,solve03,
    --gen04,solve04,
    --gen05,solve05,
    --gen06,solve06,
    --gen07,solve07,
    --gen08,solve08,
    --gen09,solve09,
    dir
) where

import Control.Monad
import System.Random

dir :: FilePath -> FilePath
dir = ("src/etc/"<>)

gen00 :: IO ()
gen00 = do
        val <- forM [1..10000] $ const (randomIO :: IO Int)
        writeFile (dir "randomNumbers.txt") . unlines . fmap (show . (`mod` 1000000)) $ val

sort :: Ord a => [a] -> [a]
sort (x:xs) = (sort . filter (<=x)) xs ++ [x] ++ (sort . filter (>x)) xs
sort [] = []

solve00 :: IO ()
solve00 = do
        c <- readFile (dir "randomNumbers.txt")
        writeFile (dir "randomNumbersSorted.txt") . unlines . fmap show . sort . fmap (read :: String -> Int) . lines $ c
