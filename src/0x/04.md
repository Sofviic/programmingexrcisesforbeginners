# higher or lower game (define CONSTs MIN & MAX)

### Task 4.a. 
define 2 constants:
1. MIN (= 1)
2. MAX (= 10)

then generate a random number (call it the *answer*) between MIN & MAX, then make a loop that:
1. prompts user for number,
2. if number equals *answer*, print "You Win!", and exit the loop.
3. if number is smaller than *answer*, print "too small", and go back to the start of the loop.
3. if number is bigger than *answer*, print "too big", and go back to the start of the loop.
### Task 4.b. 
Make it so that you don't have to restart the program to start a new game.
### Task 4.c. 
Make it so that when a new game starts, the user can choose the MIN & MAX values.
### Bonus Task 4.d.
Define a constant MAXGUESSES.
Record the number of guesses, and if that number exceeds MAXGUESSES, print "XX You Lose :'( XX", and exit the game.
##### Task 4.d.ii.
On losing, additionaly print the *answer*. (so that the user knows what was the number they failed to guess)
