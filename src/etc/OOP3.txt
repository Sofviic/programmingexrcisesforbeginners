1. define Stack<T>:
    - Push(T) -> void
    - Pop() -> T
    - Length() -> int
    - IsEmpty() -> bool
2. define Queue<T>:
    - Enqueue(T) -> void
    - Dequeue() -> T
    - Length() -> int
    - IsEmpty() -> bool
3. define Dictionary<K,V>:
    - Add(K,V) -> void
    - Get(K) -> V?
    - Remove(K) -> void
    - Keys() -> K[]
    - Vals() -> V[]
    - Pairs() -> (K,V)[]
4. define LinkedList<T>:
    - Prepend(T) -> LinkedList<T>
    - Append(T) -> LinkedList<T>
    - Get(int) -> T
    - Remove(T) -> LinkedList<T>
    - Insert(int,T) -> LinkedList<T>
    - Length() -> int
    - IsEmpty() -> bool
    - IsLast() -> bool
    - InsertSorted(T,Func<T,T,bool>) -> LinkedList<T>
    - Concat(LinkedList<T>) -> LinkedList<T>
    - Map(Func<T,K>) -> LinkedList<K>
    - MConcat(LinkedList<LinkedList<T>>) -> LinkedList<T>
    - Take(int) -> LinkedList<T>
    - Drop(int) -> LinkedList<T>
    - Cycle() -> LinkedList<T>
5. define BinaryTree<T>:
    - Add(T) -> BinaryTree<T>
    - Has(T) -> bool
    - Remove(T) -> BinaryTree<T>
    - Size() -> int
    - IsEmpty() -> bool
    - IsLeaf() -> bool
    - ToArray() -> T[]

