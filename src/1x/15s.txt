---------float
based on |e-e_approx| < 10^-n
1 sigfig(s):  2.0
2 sigfig(s):  2.6666667
3 sigfig(s):  2.7083335
4 sigfig(s):  2.7180557
5 sigfig(s):  2.718254
6 sigfig(s):  2.718279
7 sigfig(s):  2.7182817
8 sigfig(s):  2.7182817
9 sigfig(s):  2.7182817
10 sigfig(s):  2.7182817

based on number of correct digits
1 sigfig(s):  2.0
2 sigfig(s):  2.7083335
3 sigfig(s):  2.716667
4 sigfig(s):  2.7180557
5 sigfig(s):  2.718254
6 sigfig(s):  2.7182817
7 sigfig(s):  2.7182817
8 sigfig(s):  2.7182817
9 sigfig(s):  2.7182817
10 sigfig(s):  2.7182817

---------double
based on |e-e_approx| < 10^-n
1 sigfig(s):  2.0
2 sigfig(s):  2.6666666666666665
3 sigfig(s):  2.708333333333333
4 sigfig(s):  2.7180555555555554
5 sigfig(s):  2.7182539682539684
6 sigfig(s):  2.71827876984127
7 sigfig(s):  2.7182815255731922
8 sigfig(s):  2.7182818011463845
9 sigfig(s):  2.718281826198493
10 sigfig(s):  2.7182818282861687

based on number of correct digits
1 sigfig(s):  2.0
2 sigfig(s):  2.708333333333333
3 sigfig(s):  2.7166666666666663
4 sigfig(s):  2.7180555555555554
5 sigfig(s):  2.7182539682539684
6 sigfig(s):  2.7182815255731922
7 sigfig(s):  2.7182815255731922
8 sigfig(s):  2.7182818011463845
9 sigfig(s):  2.718281826198493
10 sigfig(s):  2.7182818282861687

---------
first 41 digits of e
2.7182818284590452353602874713526624977572...
