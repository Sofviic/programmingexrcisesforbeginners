module E1x (
    --gen10,solve10,
    gen11,solve11,
    gen12,solve12,
    solve13,
    gen14,solve14,
    solve15,
    --gen16,solve16,
    --gen17,solve17,
    --gen18,solve18,
    --gen19,solve19,
    dir
) where

import Data.List (intercalate,transpose)
import Data.Char (toUpper,toLower)
import Data.List.Split (splitOn,chunksOf)
import Data.Function (on)
import Control.Monad (forM,liftM,liftM2)
import System.Random (randomIO,randomRIO)
import System.Directory (listDirectory,doesDirectoryExist)
import System.FilePath (takeFileName,normalise,(</>))

dir :: FilePath -> FilePath
dir = ("src/1x/"<>)

morse :: Char -> String
morse 'A' = ".-"
morse 'B' = "-..."
morse 'C' = "-.-."
morse 'D' = "-.."
morse 'E' = "."
morse 'F' = "..-."
morse 'G' = "--."
morse 'H' = "...."
morse 'I' = ".."
morse 'J' = ".---"
morse 'K' = "-.-"
morse 'L' = ".-.."
morse 'M' = "--"
morse 'N' = "-."
morse 'O' = "---"
morse 'P' = ".--."
morse 'Q' = "--.-"
morse 'R' = ".-."
morse 'S' = "..."
morse 'T' = "-"
morse 'U' = "..-"
morse 'V' = "...-"
morse 'W' = ".--"
morse 'X' = "-..-"
morse 'Y' = "-.--"
morse 'Z' = "--.."
unmorse :: String -> Char
unmorse ".-"    =  'A' 
unmorse "-..."  =  'B' 
unmorse "-.-."  =  'C' 
unmorse "-.."   =  'D' 
unmorse "."     =  'E' 
unmorse "..-."  =  'F' 
unmorse "--."   =  'G' 
unmorse "...."  =  'H' 
unmorse ".."    =  'I' 
unmorse ".---"  =  'J' 
unmorse "-.-"   =  'K' 
unmorse ".-.."  =  'L' 
unmorse "--"    =  'M' 
unmorse "-."    =  'N' 
unmorse "---"   =  'O' 
unmorse ".--."  =  'P' 
unmorse "--.-"  =  'Q' 
unmorse ".-."   =  'R' 
unmorse "..."   =  'S' 
unmorse "-"     =  'T' 
unmorse "..-"   =  'U' 
unmorse "...-"  =  'V' 
unmorse ".--"   =  'W' 
unmorse "-..-"  =  'X' 
unmorse "-.--"  =  'Y' 
unmorse "--.."  =  'Z' 

morseS, unmorseS :: String -> String
morseS = intercalate "   " . fmap (intercalate " ") . (fmap.fmap) morse . words
unmorseS = intercalate " " . (fmap.fmap) unmorse . fmap (splitOn " ") . splitOn "   "

singleSpaceOnly :: String -> String
singleSpaceOnly = unwords . words

gen11 :: IO ()
gen11 = do
        s <- readFile $ dir "11_.txt"
        writeFile (dir "11.txt") . singleSpaceOnly . filter (`elem` ' ':['A'..'Z']) . fmap toUpper $ s

solve11 :: IO ()
solve11 = do
        s <- readFile $ dir "11.txt"
        if (unmorseS . morseS) s == id s
                then putStrLn "successful encode-decode" 
                else error "no longer the same as original file"
        writeFile (dir "11s.txt") $ morseS s
        putStrLn . morseS $ "HELLO I AM A SAMPLE TEXT FOR THE PURPOSES OF EXAMPLIFICATION"

matrixA, matrixB :: Int -> Int
matrixA 0 = 64
matrixA 1 = 32
matrixB 0 = matrixA 1
matrixB 1 = 16
matrixADim,matrixBDim :: Int
matrixADim = product $ fmap matrixA [0,1]
matrixBDim = product $ fmap matrixB [0,1]

shapify :: (Int -> Int) -> [a] -> [[a]]
shapify = chunksOf . ($ 1)

showBlock :: Show a => [[a]] -> String
showBlock = unlines . fmap unwords . (fmap.fmap) show

readBlock :: Read a => String -> [[a]]
readBlock = (fmap.fmap) read . fmap words . lines

gen12 :: IO ()
gen12 = do
        valA <- forM [1..matrixADim] $ const (randomIO :: IO Int)
        valB <- forM [1..matrixBDim] $ const (randomIO :: IO Int)
        let matA = showBlock . shapify matrixA . fmap (`mod` 100) $ valA
        let matB = showBlock . shapify matrixB . fmap (`mod` 100) $ valB
        writeFile (dir "12.txt") $ matA <> "\n" <> matB

matrixProduct :: [[Int]] -> [[Int]] -> [[Int]]
matrixProduct a b = fmap (($ transpose b) . fmap . (sum .) . zipWith (*)) a

solve12 :: IO ()
solve12 = do
        s <- fmap lines . readFile $ dir "12.txt"
        let matA = takeWhile (/= mempty) s
        let matB = drop 1 $ dropWhile (/= mempty) s
        writeFile (dir "12s.txt") . showBlock $ matrixProduct ((readBlock . unlines) matA) ((readBlock . unlines) matB)

data FileTree = Directory String [FileTree] | File String | Hole deriving Show

buildFileTree :: FilePath -> IO FileTree
buildFileTree fp = doesDirectoryExist fp >>= \b -> case b of
                False -> return . File $ fp
                True -> do
                        files <- listDirectory $ fp
                        fmap (Directory fp) . sequence . fmap buildFileTree . fmap (fp</>) $ files

showFileTree :: FileTree -> String
showFileTree (File s) = (takeFileName.normalise) s
showFileTree (Directory s ft) = (takeFileName.normalise) s <> "\n" <> mconcat (fmap (tabWith "\t" . showFileTree) ft)
showFileTree _ = ""

notPureWhiteSpace :: String -> Bool
notPureWhiteSpace = any (`notElem` " \n\t")

tabWith :: String -> String -> String
tabWith t = unlines . fmap (t<>) . lines

-- TODO: change so that only the last tab is `f`, the rest are `t`.
showFileTreeWith :: String -> String -> FileTree -> String
showFileTreeWith _ f (File s) = f <> (takeFileName.normalise) s
showFileTreeWith t f (Directory s ft) = (takeFileName.normalise) s <> "\n" <> mconcat (fmap (tabWith t . showFileTreeWith t f) ft)
showFileTreeWith _ _ _ = ""

filterOutNamed :: [String] -> FileTree -> FileTree
filterOutNamed ns (Directory s _) | (takeFileName.normalise) s `elem` ns = Hole
filterOutNamed ns (File s)        | (takeFileName.normalise) s `elem` ns = Hole
filterOutNamed ns (Directory s ft) = Directory s $ fmap (filterOutNamed ns) ft
filterOutNamed _  x = x

solve13 :: IO ()
solve13 = (fmap (showFileTree . filterOutNamed [".git",".vscode",".stack-work"]) . buildFileTree $ ".") >>= writeFile (dir "13s.txt")

isAlphaNum :: Char -> Bool
isAlphaNum = (`elem` ['a'..'z'] <> ['A'..'Z'])

genWordBank :: FilePath -> FilePath -> IO ()
genWordBank src dest = fmap (unlines . filter (/= mempty) . fmap (fmap toLower . filter isAlphaNum) . words) (readFile src) >>= writeFile dest

type Answer = String

hideWord :: Answer -> String
hideWord = fmap $ const '_'

guessLetter :: Char -> Answer -> String -> String
guessLetter c = zipWith $ \ac sc -> if ac == c then c else sc

iterateMaybeM :: Monad m => a -> (a -> m (Maybe a)) -> m [a]
iterateMaybeM ox f = do
    mx <- f ox
    case mx of
        Nothing -> pure []
        Just x -> (x:) <$> iterateMaybeM x f

gen14 :: IO ()
gen14 = do
        genWordBank (dir "14wordbank_.txt") (dir "14wordbank.txt")
        wordbank <- lines <$> readFile (dir "14wordbank.txt")
        guessWordsIndices <- forM [1..10] $ const (randomRIO (0,length wordbank-1) :: IO Int)
        let guessWords = (wordbank !!) <$> guessWordsIndices
        writeFile (dir "14.txt") ""
        forM [0..length guessWords - 1] $ \i -> do
                let guessWord = guessWords !! i
                appendFile (dir "14.txt") $ "GameState: " <> hideWord guessWord <> "\n"
                numOfGuesses <- randomRIO (0,25) :: IO Int
                iterateMaybeM (hideWord guessWord,numOfGuesses) $ \(currentWord,numOfGuessesLeft) ->
                        if numOfGuessesLeft < 1
                                then pure Nothing
                                else do
                                        c <- randomRIO ('a','z') :: IO Char
                                        let newState = guessLetter c guessWord currentWord
                                        appendFile (dir "14.txt") $ "Guessing: " <> [c] <> "\n"
                                        appendFile (dir "14.txt") $ "GameState: " <> newState <> "\n"
                                        return $ Just (newState, numOfGuessesLeft-1)
                appendFile (dir "14.txt") "\n"
        return ()

-- _ is counted as blank and matches any character
valid :: Answer -> String -> Bool
valid (_:mr) ('_':sr) = valid mr sr
valid (m:mr) (s:sr)   = if m == s then valid mr sr else False
valid [] [] = True
valid _ _ = False

allValid :: Answer -> [String] -> [String]
allValid = filter . valid

fileBlocks :: String -> [[String]]
fileBlocks = splitOn mempty . lines

solve14 :: IO ()
solve14 = do
        wordbank <- lines <$> readFile (dir "14wordbank.txt")
        undefined --TODO

till :: (a -> b -> b) -> b -> (b -> Bool) -> [a] -> b
till op s f [] = s
till op s f (a:as) = if f s then s else till op (op a s) f as

sumTill :: Num a => (a -> Bool) -> [a] -> a
sumTill = till (+) 0

fflap :: Functor f => a -> f (a -> b) -> f b
fflap a f = fmap ($ a) f

sigfigs :: (Ord a, Floating a) => a -> a -> a -> Bool
sigfigs n a b = abs (a - b) < 10**(1-n)

stringfigs :: Show a => Int -> a -> a -> Bool
stringfigs n a b = on (==) (take n . show) a b

approxE :: (Ord a, Floating a, Show a, Enum a) => [a]
approxE = fflap ((recip . product . enumFromTo 1) <$> [0..])
        $ sumTill 
        . flip sigfigs (exp 1)
        <$> [1..]

approxE' :: (Ord a, Floating a, Show a, Enum a) => [a]
approxE' = fflap ((recip . product . enumFromTo 1) <$> [0..])
        $ sumTill 
        . flip stringfigs (exp 1)
        <$> [2..]

solve15 :: IO ()
solve15 = do
        writeFile (dir "15s.txt") ""
        appendFile (dir "15s.txt") $ "---------float\n"
        appendFile (dir "15s.txt") $ "based on |e-e_approx| < 10^-n\n"
        appendFile (dir "15s.txt") . unlines . zipWith (\i d -> show i <> " sigfig(s):  " <> show d) [1..10] $ (approxE :: [Float])
        appendFile (dir "15s.txt") $ "\n"
        appendFile (dir "15s.txt") $ "based on number of correct digits\n"
        appendFile (dir "15s.txt") . unlines . zipWith (\i d -> show i <> " sigfig(s):  " <> show d) [1..10] $ (approxE' :: [Float])
        appendFile (dir "15s.txt") $ "\n"
        appendFile (dir "15s.txt") $ "---------double\n"
        appendFile (dir "15s.txt") $ "based on |e-e_approx| < 10^-n\n"
        appendFile (dir "15s.txt") . unlines . zipWith (\i d -> show i <> " sigfig(s):  " <> show d) [1..10] $ (approxE :: [Double])
        appendFile (dir "15s.txt") $ "\n"
        appendFile (dir "15s.txt") $ "based on number of correct digits\n"
        appendFile (dir "15s.txt") . unlines . zipWith (\i d -> show i <> " sigfig(s):  " <> show d) [1..10] $ (approxE' :: [Double])
        appendFile (dir "15s.txt") $ "\n"
        appendFile (dir "15s.txt") $ "---------\n"
        appendFile (dir "15s.txt") $ "first 41 digits of e\n"
        appendFile (dir "15s.txt") $ "2.7182818284590452353602874713526624977572...\n"
