# Exercises
## 0x
01. [link to task 01](../src/0x/01.md). [fibonacci](https://en.wikipedia.org/wiki/Fibonacci_sequence)
02. [link to task 02](../src/0x/02.md). random string generator
03. [link to task 03](../src/0x/03.md). how many times does a character occur in a string ([file provided](../src/0x/03.txt))
04. [link to task 04](../src/0x/04.md). higher or lower game (define CONSTs MIN & MAX)
05. [link to task 05](../src/0x/05.md). calculate the sum of numbers in a text file (one line per number) ([file provided](../src/0x/05.txt))
06. [link to task 06](../src/0x/06.md). calculate the sum of products of numbers in a text file (2 numbers per line) ([file provided](../src/0x/06.txt))
07. [link to task 07](../src/0x/07.md). check where 2 text files have different lines ([file a](../src/0x/07a.txt) & [file b](../src/0x/07b.txt))
08. [link to task 08](../src/0x/08.md). sort lines of a file in [ascii order](https://www.asciitable.com/) ([file provided](../src/0x/08.txt))
09. [link to task 09](../src/0x/09.md). make compression & decompression algorithm using [run length encoding](https://en.wikipedia.org/wiki/Run-length_encoding) ([test file provided](../src/0x/09.txt))
## 1x
10. [link to task 10](../src/1x/10.md). make a timer program
11. [link to task 11](../src/1x/11.md). make a morse code encoder/decoder ([test file provided](../src/1x/11.txt))
12. [link to task 12](../src/1x/12.md). matrix multiplication ([test file provided](../src/1x/12.txt))
13. [link to task 13](../src/1x/13.md). make a tool that prints out the [tree structure of a directory](https://en.wikipedia.org/wiki/Tree_(command))
14. [link to task 14](../src/1x/14.md). ($\textcolor{red}{\text{Under Construction}}$) write a [hangman](https://en.wikipedia.org/wiki/Hangman_(game)) ai
15. [link to task 15](../src/1x/15.md). compute [$e$](https://en.wikipedia.org/wiki/E_(mathematical_constant)) using the [taylor series](https://en.wikipedia.org/wiki/Taylor_series) expansion of [$e^x$](https://en.wikipedia.org/wiki/Exponential_function) upto 6 [significant figures](https://en.wikipedia.org/wiki/Significant_figures)
15. [link to task 16](../src/1x/16.md). ($\textcolor{red}{\text{Under Construction}}$) create a primative physics engine that handles collisions of two circles
